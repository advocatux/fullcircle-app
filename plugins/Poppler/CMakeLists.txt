set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(PLUGIN "Poppler")

set(CMAKE_AUTOMOC ON)

find_package(Qt5Core)
find_package(Qt5Gui)
find_package(Qt5Qml)
find_package(Qt5Quick)
find_package(Qt5Xml)
find_package(Qt5Concurrent)

include_directories(
	${CMAKE_CURRENT_SOURCE_DIR}
	${CMAKE_CURRENT_BINARY_DIR}
	${Qt5Quick_PRIVATE_INCLUDE_DIRS}
	${Qt5Qml_PRIVATE_INCLUDE_DIRS}
)

set(SRC
    plugin.cpp
    pdfdocument.cpp
    pdfimageprovider.cpp
    pdfitem.cpp
    verticalview.cpp
    pdftocmodel.cpp
)

add_library(${PLUGIN} MODULE ${SRC})
set_target_properties(${PLUGIN} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PLUGIN})
target_link_libraries(${PLUGIN} poppler-qt5)
qt5_use_modules(${PLUGIN} Gui Qml Quick Xml Concurrent)

execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
    OUTPUT_VARIABLE ARCH_TRIPLET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
set(QT_IMPORTS_DIR "/lib/${ARCH_TRIPLET}")

install(TARGETS ${PLUGIN} DESTINATION ${QT_IMPORTS_DIR}/${PLUGIN}/)
install(FILES qmldir DESTINATION ${QT_IMPORTS_DIR}/${PLUGIN}/)
install(FILES ViewDelegate.qml DESTINATION ${QT_IMPORTS_DIR}/${PLUGIN}/)
